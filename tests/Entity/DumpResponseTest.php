<?php

namespace Farvest\DatabaseDumpBundle\Tests\Entity;

use Farvest\DatabaseDumpBundle\Entity\DumpResponse;
use PHPUnit\Framework\TestCase;

class DumpResponseTest extends TestCase
{
    const STR_STATUS = 'ok';
    const STR_FILENAME = 'test.html';
    const STR_MESSAGE = 'a new message';
    const STR_ERROR = 'a big error';

    const ATTR_STRING = 'string';
    const ATTR_ARRAY = 'array';

    public function testGetStatus()
    {
        $dumpResponse = new DumpResponse('','','','');
        $dumpResponse->setStatus(self::STR_STATUS);
        $this->assertInternalType(self::ATTR_STRING, $dumpResponse->getStatus());
        $this->assertEquals(self::STR_STATUS, $dumpResponse->getStatus());
    }

    public function testGetFilename()
    {
        $dumpResponse = new DumpResponse('','','','');
        $dumpResponse->setFilename(self::STR_FILENAME);
        $this->assertInternalType(self::ATTR_STRING, $dumpResponse->getFilename());
        $this->assertEquals(self::STR_FILENAME, $dumpResponse->getFilename());
    }

    public function testGetMessage()
    {
        $dumpResponse = new DumpResponse('','','','');
        $dumpResponse->setMessage(self::STR_MESSAGE);
        $this->assertInternalType(self::ATTR_STRING, $dumpResponse->getMessage());
        $this->assertEquals(self::STR_MESSAGE, $dumpResponse->getMessage());
    }

    public function testGetError()
    {
        $dumpResponse = new DumpResponse('','','','');
        $dumpResponse->setError(self::STR_ERROR);
        $this->assertInternalType(self::ATTR_STRING, $dumpResponse->getError());
        $this->assertEquals(self::STR_ERROR, $dumpResponse->getError());
    }

    public function testGetArray()
    {
        $dumpResponse = new DumpResponse(
            self::STR_STATUS,
            self::STR_FILENAME,
            self::STR_MESSAGE,
            self::STR_ERROR
        );

        $expected = [
            'status'    => self::STR_STATUS,
            'filename'  => self::STR_FILENAME,
            'message'   => self::STR_MESSAGE,
            'error'     => self::STR_ERROR
        ];

        $this->assertInternalType(self::ATTR_ARRAY, $dumpResponse->getArray());
        $this->assertEquals($expected, $dumpResponse->getArray());
    }
}