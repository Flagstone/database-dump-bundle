<?php


namespace Farvest\DatabaseDumpBundle\Tests\Service;


use Farvest\DatabaseDumpBundle\Service\FileSystem;
use Farvest\DatabaseDumpBundle\Service\Exceptions\FileSystemExceptions\DirectoryNotCreatedException;
use Farvest\DatabaseDumpBundle\Service\Exceptions\FileSystemExceptions\InvalidDirectoryParameterException;
use Farvest\DatabaseDumpBundle\Service\Exceptions\FileSystemExceptions\FileNotExistsException;
use org\bovigo\vfs\vfsStream;
use PHPUnit\Framework\TestCase;

class FileSystemTest extends TestCase
{
    /**
     * @var FileSystem
     */
    private $fs;

    /**
     * @var vfsStreamDirectory
     */
    private $root;

    protected function setUp()
    {
        $this->fs = new FileSystem();
        $this->root = vfsStream::setUp('exampleDir');
    }

    /**
     * @throws DirectoryNotCreatedException
     * @throws InvalidDirectoryParameterException
     */
    public function testCreateDirectoryWithDefaultPermissions()
    {
        $this->root->chmod(0700);
        //  Test that there is no folder 'newDir' in the structure
        $this->assertFalse($this->root->hasChild('newDir'));
        //  Create a directory with default permissions
        $this->fs->createDirectory(vfsStream::url('exampleDir').'/newDir');
        //  Test if the folder is created
        $this->assertTrue($this->root->hasChild('newDir'));
        //  Test if permissions are good
        $this->assertEquals(0700, $this->root->getChild('newDir')->getPermissions());
        //  Test if true is returned when a folder is already created
        $this->assertTrue($this->fs->createDirectory(vfsStream::url('exampleDir').'/newDir'));
    }

    /**
     * @throws DirectoryNotCreatedException
     * @throws InvalidDirectoryParameterException
     */
    public function testCreateDirectoryWithPermissions()
    {
        $this->root->chmod(0700);
        //  Create a directory with setting permissions
        $this->fs->createDirectory(vfsStream::url('exampleDir').'/newDir', 0777);
        //  Test if the folder is created
        $this->assertTrue($this->root->hasChild('newDir'));
        //  Test if permissions are good
        $this->assertEquals(0777, $this->root->getChild('newDir')->getPermissions());
    }

    /**
     *  @expectedException
     *  @test
     */
    public function testCreateDirectoryPermissionsException()
    {
        $this->root->chmod(0400);
        //  Test if the folder cannot be created if parent permissions are not sufficient
        $this->expectException(DirectoryNotCreatedException::class);
        $this->fs->createDirectory(vfsStream::url('exampleDir').'/newDir');
    }

    /**
     *  @expectedException
     *  @test
     */
    public function testCreateDirectoryEmptyParameterException()
    {
        $this->root->chmod(0700);
        //  Test if the folder cannot be created if folder is empty
        $this->expectException(InvalidDirectoryParameterException::class);
        $this->fs->createDirectory('');
    }

    /**
     *  @expectedException
     *  @test
     */
    public function testCreateDirectoryNullParameterException()
    {
        $this->root->chmod(0700);
        //  Test if the folder cannot be created if folder is null
        $this->expectException(InvalidDirectoryParameterException::class);
        $this->fs->createDirectory(null);
    }

    /**
     * @throws FileNotExistsException
     */
    public function testReadAndDeleteErrorFileWithDefaultGlue()
    {
        $this->root->chmod(0700);
        $this->root->addChild(vfsStream::newFile('testFile.txt'));

        $fileName = vfsStream::url($this->root->getChild('testFile.txt')->path());
        $file = fopen($fileName, 'a');
        fwrite($file, "line1\n");
        fwrite($file, "line2\n");
        fwrite($file, "line3");
        fclose($file);

        $file = $this->fs->readAndDeleteErrorFile($fileName);
        //  Test if the content of the file has been formatted correctly
        $this->assertInternalType('string', $file);
        $this->assertEquals('line1<br />line2<br />line3', $file);
        //  Test if the file has been deleted
        $this->assertFalse($this->root->hasChild('testFile.txt'));
    }

    /**
     * @throws FileNotExistsException
     */
    public function testReadAndDeleteErrorFileWithGlue()
    {
        $this->root->chmod(0700);
        $this->root->addChild(vfsStream::newFile('testFile.txt'));

        $fileName = vfsStream::url($this->root->getChild('testFile.txt')->path());
        $file = fopen($fileName, 'a');
        fwrite($file, "line1\n");
        fwrite($file, "line2\n");
        fwrite($file, "line3");
        fclose($file);

        $file = $this->fs->readAndDeleteErrorFile($fileName, '-');
        //  Test if the content of the file has been formatted correctly
        $this->assertEquals('line1-line2-line3', $file);
    }

    /**
     *  @expectedException
     *  @test
     */
    public function testFileNotExistsException()
    {
        $this->root->chmod(0700);
        //  Test if the folder cannot be created if folder is null
        $this->expectException(FileNotExistsException::class);
        $file = $this->fs->readAndDeleteErrorFile('fileNotExists.txt');
    }

    protected function tearDown()
    {
        unset($this->fs);
    }
}