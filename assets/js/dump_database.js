
const $ = require('jquery')
require('bootstrap')

$(document).ready(function () {
  /**
   *  Dump a database and all uploaded files
   */
  $('.data-dump').on('click', function() {
    /*
     *  Get all sentence for translated informations
     */
    let dumpInProgressTitle = $(this).data('title_dump_in_progress')
    let dumpInProgressText = $(this).data('text_dump_in_progress')
    let dumpOk = $(this).data('title_dump_ok')
    let dumpOkText= $(this).data('text_dump_ok')
    let dumpError = $(this).data('title_dump_error')
    let makeUrl = $(this).data('make_url')
    /*
     *  Show the modal to display informations
     */
    let modalDump = $('#dumps')
    modalDump.modal({
      backdrop: 'static'
    }).show()
    /*
     * Action on click on 'Save' button
     */
    let makeDumpButton = $('#make-dump')
    let makeDumpTitle = $('#dumpsLabel')
    let makeDumpText = $('.modal-body.dump p')
    let modalFooter = $('.modal-footer')

    makeDumpButton.on('click', function () {
      makeDumpButton.hide()
      modalFooter.hide()
      makeDumpTitle.text(dumpInProgressTitle)
      makeDumpText.text(dumpInProgressText)

      $.ajax({
        url: makeUrl,
        dataType: 'json',
        type: 'post',
        success: function (data) {
          if (data.status === 'ok') {
            makeDumpTitle.html('<i class="dump-ok far fa-check-circle"></i> <span class="dump-ok">'+dumpOk+'</span>');
            makeDumpText.html(dumpOkText.replace('%dump_name%', data.filename));
            modalFooter.show();
          } else {
            makeDumpTitle.html('<i class="dump-error fas fa-exclamation-triangle"></i> <span class="dump-error">'+dumpError+'</span>');
            makeDumpText.html('<p class="error-message">' + data.message + '</p><p class="error-error">' + data.error + '</p>');
            modalFooter.show();
          }
        },
        error: function (jqXHR, textStatus, errorThrown) {
          makeDumpTitle.text(dumpError)
          makeDumpText.html('<p class="error-message">' + textStatus + '</p><p class="error-error">' + errorThrown + '</p>');
          modalFooter.show()
        }
      })
    })
    /*
     * Action on click on 'Close' button
     */
    $('#modal-close').on('click', function () {
      modalDump.modal('hide')
      location.reload()
    })
    return false;
  })
  /*
   *  Delete a dump.
   */
  $('.button.delete').on('click', function () {
    $('#remove-dump-link').attr('href', $(this).data('dump'))
    $('.modal-body.remove').html($('.modal-body.remove').html().replace('%save_name%', $(this).data('file')))
    $('#dumpRemove').modal('show')
  })
  /*
   *  Upload a dump.
   */
  $('.button.data-upload').on('click', function () {
    let uploadUrl = $(this).data('upload_url')
    let uploadInProgressTitle = $(this).data('title_upload_in_progress')
    let uploadInProgressText = $(this).data('text_upload_in_progress')
    let titleMade = $(this).data('titlemade')
    let textMade = $(this).data('textmade')
    let titleError = $(this).data('titleerror')

    $('#dumpUpload').modal({
      backdrop: 'static'
    }).modal('show')

    $('#uploadFile').on('change', function(e) {
      $(this).siblings('label').text(e.target.files[0].name)
    })

    let uploadDumpButton = $('#upload-dump')
    let uploadDumpTitle = $('#uploadLabel')
    let uploadDumpText = $('.modal-body.upload p')
    let modalFooter = $('.modal-footer')

    uploadDumpButton.on('click', function () {
      uploadDumpButton.hide()
      $('.custom-file').hide()
      modalFooter.hide()
      uploadDumpTitle.text(uploadInProgressTitle)
      uploadDumpText.text(uploadInProgressText)

      let fileData = $("#uploadFile").prop('files')[0];
      let formData = new FormData();
      formData.append("file", fileData)
      $.ajax({
        url: uploadUrl,
        dataType: 'json',
        data: formData,
        type: 'post',
        cache: false,
        processData: false,
        contentType: false,
        success: function(data) {
          if (data.status === 'ok') {
            $('#uploadDumpLabel').html('<i class="dump-ok far fa-check-circle"></i> <span class="dump-ok">' + titleMade + '</span>')
            $('.modal-body.upload').text(textMade)
            $('.modal-footer').show()
          } else {
            $('#uploadDumpLabel').html('<i class="dump-error fas fa-exclamation-triangle"></i> <span class="dump-error">' + titleError + '</span>')
            $('.modal-body.upload').html('<p class="error-message">'+data.message+'</p><p class="error-error">' + data.error+'</p>')
            $('.modal-footer').show()
          }
        },
        error: function(jqXHR, textStatus, errorThrown) {
          $('#uploadDumpLabel').html('<i class="dump-error fas fa-exclamation-triangle"></i> <span class="dump-error">' + titleError + '</span>')
          $('.modal-body.upload').html('<p class="error-message">'+textStatus+'</p><p class="error-error">' + errorThrown+'</p>')
          $('.modal-footer').show()
        }
      })
    })

    $('#modal-upload-close').on('click', function() {
      $('#dumpUpload').modal('hide')
      location.reload()
    })
  })

  /*
   *  Restore a dump.
   */
  $('.button.restore').on('click', function () {
    let titleInProgress = $(this).data('titleinprogress')
    let textInProgress = $(this).data('textinprogress')
    let titleMade = $(this).data('titlemade')
    let textMade = $(this).data('textmade')
    let titleError = $(this).data('titleerror')
    let restoreUrl = $(this).data('restore_url')

    $('#dumpRestore').modal({
      backdrop: 'static'
    }).modal('show')

    $('#restore-dump').on('click', function() {
      $('.modal-footer').hide()
      $('#restoreDumpLabel').text(titleInProgress)
      $('.modal-body.restore').text(textInProgress)
      $('#restore-dump').hide()

      $.ajax({
        url: restoreUrl,
        dataType: 'json',
        type: 'post',
        success: function(data) {
          if (data.status === 'ok') {
            $('#restoreDumpLabel').html('<i class="dump-ok far fa-check-circle"></i> <span class="dump-ok">' + titleMade + '</span>')
            $('.modal-body.restore').text(textMade)
            $('.modal-footer').show()
          } else {
            $('#restoreDumpLabel').html('<i class="dump-error fas fa-exclamation-triangle"></i> <span class="dump-error">' + titleError + '</span>')
            $('.modal-body.restore').html('<p class="error-message">'+data.message+'</p><p class="error-error">' + data.error+'</p>')
            $('.modal-footer').show()
          }
        },
        error: function(jqXHR, textStatus, errorThrown) {
          $('#restoreDumpLabel').html('<i class="dump-error fas fa-exclamation-triangle"></i> <span class="dump-error">' + titleError + '</span>')
          $('.modal-body.restore').html('<p class="error-message">'+textStatus+'</p><p class="error-error">' + errorThrown+'</p>')
          $('.modal-footer').show()
        }
      })
    })

    $('#modal-restore-close').on('click', function() {
      $('#dumps').modal('hide')
      location.reload()
    })
  })
})