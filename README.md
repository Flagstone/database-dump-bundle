# Flagstone Database Dump Bundle

This bundle allows you to perform backups and restores of an application's database, as well as documents downloaded by users, whose links are stored in the database.

## Installation

Use composer manager to install it
```bash
composer install flagstone/database-dump-bundle
```

## Import Routes

In the routing file of your application, add the following
```yaml
flagstone_database_dump:
    resource: '@FlagstoneDatabaseDumpBundle\Resources\routes.yaml'
    prefix: '/admin'  # optional, only if you want to add this at an admin level 
```
or this for multi-language website
```yaml
flagstone_database_dump:
    resource: '@FlagstoneDatabaseDumpBundle\Resources\routes_intl.yaml'
    prefix: '/admin'  # optional, only if you want to add this at an admin level
```
## Configuration
Add Security rules (to security.yaml, access_control) for the access to dump/ route (for example):
```yaml
- { path: ^/dump, role: ROLE_ADMIN }
```
Add globals in twig.yaml
```yaml
twig
    globals:
        database_dump: "@Flagstone\\DatabaseDumpBundle\\Service\\DatabaseDump"
```

List of the parameters you can override (for exemple):
```yaml
    database_dump_logDir: '%kernel.project_dir%/log'
    database_dump_uploadDirs:
        'qrcodes': '/public/uploads/qrcodes'
        'translations': '/translations'
    database_dump_dumpDir: 'database-dump/'
    database_dump_migrationsDir: '/migrations'
```
Make attention that `database_dump_dumpDir` parameter shouldn't begin with a slash. 

Add the database dump directory in your .gitignore of your application.

## License

[MIT](https://choosealicence.com/licenses/mit/)
