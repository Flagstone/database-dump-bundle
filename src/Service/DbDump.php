<?php
/** *****************************************************************************************************************
 *  DbDump.php
 *  *****************************************************************************************************************
 *  @copyright 2022 Flagstone
 *  @author Emmanuel Grosdemange <emmanuel.grosdemange57@gmail.com>
 *  *****************************************************************************************************************
 *  Created: 2019/04/22
 *  ***************************************************************************************************************** */

namespace Flagstone\DatabaseDumpBundle\Service;

use Flagstone\ActionLoggingBundle\Entity\Log;
use Flagstone\ActionLoggingBundle\FlagstoneActionLoggingBundle;
use Flagstone\ActionLoggingBundle\Service\Logging;
use Flagstone\DatabaseDumpBundle\FlagstoneDatabaseDumpBundle;
use Flagstone\DatabaseDumpBundle\Service\Exceptions\FileSystemExceptions\DirectoryNotCreatedException;
use Flagstone\DatabaseDumpBundle\Service\Exceptions\FileSystemExceptions\InvalidDirectoryParameterException;
use Flagstone\DatabaseDumpBundle\Service\Exceptions\FileSystemExceptions\FileNotExistsException;
use Exception;

/** *****************************************************************************************************************
 *  Class DbDump
 *  -----------------------------------------------------------------------------------------------------------------
 *  Dump and restore database
 *  -----------------------------------------------------------------------------------------------------------------
 *  @package Flagstone\DatabseDump\Service
 *  ***************************************************************************************************************** */
class DbDump
{
    private Logging $logger;
    private Log $log;
    private ExecCommandError $error;
    private string $dbHost;
    private string $dbUser;
    private string $dbPassword;
    private string $dbName;

    /** *************************************************************************************************************
     *  DbDump constructor.
     *  -------------------------------------------------------------------------------------------------------------
     *  @param Logging $logger
     *  @param ExecCommandError $error
     *  @param array $dumpInfos
     *  ************************************************************************************************************* */
    public function __construct(Logging $logger, ExecCommandError $error, array $dumpInfos)
    {
        $this->logger = $logger;
        $this->error = $error;
        $this->dbHost = $dumpInfos['db_host'];
        $this->dbUser = $dumpInfos['db_user'];
        $this->dbPassword = urldecode($dumpInfos['db_password']);
        $this->dbName = $dumpInfos['db_name'];
        $this->log = new Log();
        $this->log->setContext(FlagstoneDatabaseDumpBundle::getContext());
    }

    /** *************************************************************************************************************
     *  Create a dump of the database using mysqldump (if the database is a mysql or mariadb database of course)
     *  Error is redirected to a logfile than can be read by the ExecCommandError service.
     *  -------------------------------------------------------------------------------------------------------------
     *  @param string $dumpFilename
     *  @return string Return an empty string if ok, else, the message returned by the command
     *  @throws DirectoryNotCreatedException
     *  @throws FileNotExistsException
     *  @throws InvalidDirectoryParameterException
     *  @throws Exception
     *  ************************************************************************************************************* */
    public function backupDB(string $dumpFilename): string
    {
        $errorFilename = $this->error
            ->createLogDirIfNotExists()
            ->setErrorFilename()
            ->getFullErrorFilename();

        $execDump =
            'mysqldump -u'.$this->dbUser.
            ' -p'.$this->dbPassword.
            ' '.$this->dbName.
            ' 2> '.$errorFilename.
            ' | gzip -c > '.$dumpFilename;

        $this->log->setAction('backupDb');
        $this->logger->info($this->log,'[DbDump::backupDb] : ' . str_replace($this->dbPassword, '########', $execDump));
        exec($execDump);

        return $this->error->get();
    }

    /** *************************************************************************************************************
     *  Restore a mysql database, using zcat to read a compressed gzip file.
     *  -------------------------------------------------------------------------------------------------------------
     *  @param string $tmpDir
     *  @param string $dumpDbFile
     *  @return string Return an empty string if ok, else, the message returned by the command
     *  @throws DirectoryNotCreatedException
     *  @throws FileNotExistsException
     *  @throws InvalidDirectoryParameterException
     *  @throws Exception
     *  ************************************************************************************************************* */
    public function restoreDb(string $tmpDir, string $dumpDbFile
    ): string
    {
        $fullDumpDbFile = $tmpDir.'/'.str_replace('tar.gz', 'sql.gz', $dumpDbFile);
        $errorFilename = $this->error
            ->createLogDirIfNotExists()
            ->setErrorFilename()
            ->getFullErrorFilename();

        $execRestore =
            'zcat '.
            $fullDumpDbFile.
            ' | mysql -u'.
            $this->dbUser.' -p'.
            $this->dbPassword.' '.
            $this->dbName.
            ' 2> '.$errorFilename;

        $this->log->setAction('restoreDb');
        $this->logger->info($this->log, '[DbDump::restoreUp] : ' . str_replace($this->dbPassword, '########', $execRestore));
        exec($execRestore);

        return $this->error->get();
    }
}