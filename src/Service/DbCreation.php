<?php
/** *****************************************************************************************************************
 *  DbCreation.php
 *  *****************************************************************************************************************
 *  @copyright 2022 Flagstone
 *  @author Emmanuel Grosdemange <emmanuel.grosdemange57@gmail.com>
 *  *****************************************************************************************************************
 *  Created: 2022/04/22
 *  ***************************************************************************************************************** */

namespace Flagstone\DatabaseDumpBundle\Service;

use PDO;
use PDOException;

/** *****************************************************************************************************************
 *  Class DbCreation
 *  -----------------------------------------------------------------------------------------------------------------
 *  To create a database without Doctrine bundle
 *  -----------------------------------------------------------------------------------------------------------------
 *  @package Flagstone\DatabaseDump\Service
 *  ***************************************************************************************************************** */
class DbCreation
{
    private string $dbHost;
    private string $dbUser;
    private string $dbPassword;
    private string $dbName;
    private PDO $conn;

    /** *************************************************************************************************************
     *  DbCreation constructor.
     *  -------------------------------------------------------------------------------------------------------------
     *  @param array $dbInfos
     *  ************************************************************************************************************* */
    public function __construct(array $dbInfos)
    {
        $this->dbHost = $dbInfos['db_host'];
        $this->dbUser = $dbInfos['db_user'];
        $this->dbPassword = urldecode($dbInfos['db_password']);
        $this->dbName = $dbInfos['db_name'];

        try {
            $this->conn = new PDO('mysql:host='.$this->dbHost, $this->dbUser, $this->dbPassword);
            $this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        } catch (PDOException $e) {
            throw new PDOException($e->getMessage());
        }
    }

    /** *************************************************************************************************************
     *  Create a database with the credentials given in the service.yaml file.
     *  Then we can perform a restore from an another environement (from prod to dev for example) to test that
     *  data are good.
     *  ************************************************************************************************************* */
    public function createDatabase()
    {
        try {
            $sqlDrop = 'DROP DATABASE '.$this->dbName;
            $this->conn->exec($sqlDrop);
            $sqlCreate = 'CREATE DATABASE '.$this->dbName.' DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci';
            $this->conn->exec($sqlCreate);
        } catch (PDOException $e) {
            throw new PDOException($e->getMessage());
        }
    }
}
