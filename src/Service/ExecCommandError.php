<?php
/** *****************************************************************************************************************
 *  ExecCommandError.php
 *  *****************************************************************************************************************
 *  @copyright 2022 Flagstone
 *  @author Emmanuel Grosdemange <emmanuel.grosdemange57@gmail.com>
 *  *****************************************************************************************************************
 *  Created: 2019/04/22
 *  ***************************************************************************************************************** */

namespace Flagstone\DatabaseDumpBundle\Service;

use Exception;
use Symfony\Component\HttpKernel\KernelInterface;
use Flagstone\DatabaseDumpBundle\Service\Exceptions\FileSystemExceptions\DirectoryNotCreatedException;
use Flagstone\DatabaseDumpBundle\Service\Exceptions\FileSystemExceptions\InvalidDirectoryParameterException;
use Flagstone\DatabaseDumpBundle\Service\Exceptions\FileSystemExceptions\FileNotExistsException;

/** *****************************************************************************************************************
 *  Class ExecCommandError
 *  -----------------------------------------------------------------------------------------------------------------
 *  Catch error generate by exec command, stored in a .log file.
 *  When an 'exec' command is made, if an error occured, the result of this error is stored on a temporary log file,
 *  and this file is read by this class.
 *  -----------------------------------------------------------------------------------------------------------------
 *  @package Flagstone\DatabseDump\Service
 *  ***************************************************************************************************************** */
class ExecCommandError
{
    private KernelInterface $kernel;
    private string $logDir;
    private string $errorFileName;
    private FileSystem $fs;

    /** *************************************************************************************************************
     *  ExecCommandError constructor.
     *  -------------------------------------------------------------------------------------------------------------
     *  @param KernelInterface $kernel
     *  @param FileSystem $fs
     *  @param string $logDir
     *  @throws Exception
     *  ************************************************************************************************************* */
    public function __construct(KernelInterface $kernel, FileSystem $fs, string $logDir)
    {
        $this->kernel = $kernel;
        $this->logDir = $logDir;
        $this->fs = $fs;
    }

    /** *************************************************************************************************************
     *  Read the error file, remove the error file and send the error message.
     *  The error file is generate by DbDump or CompressDump classes.
     *  -------------------------------------------------------------------------------------------------------------
     *  @return string
     *  @throws FileNotExistsException
     *  ************************************************************************************************************* */
    public function get(): string
    {
        return $this->fs->readAndDeleteErrorFile($this->getFullErrorFilename());
    }

    /** *************************************************************************************************************
     *  Create a random file name for the error filename.
     *  -------------------------------------------------------------------------------------------------------------
     *  @return ExecCommandError
     *  @throws Exception
     *  ************************************************************************************************************* */
    public function setErrorFilename(): self
    {
        $this->errorFileName = 'error_'.bin2hex(random_bytes(5)).'.log';
        return $this;
    }

    /** *************************************************************************************************************
     *  Get the full string of the error filename (Folder and File). If the folder doesn't exist it is created.
     *  -------------------------------------------------------------------------------------------------------------
     *  @return string
     *  ************************************************************************************************************* */
    public function getFullErrorFilename(): string
    {
        return $this->getLogDir().'/'.$this->errorFileName;
    }

    /** *************************************************************************************************************
     *  @return string
     *  ************************************************************************************************************* */
    public function getLogDir(): string
    {
        return $this->logDir;
    }

    /** *************************************************************************************************************
     *  Create the log directory.
     *  -------------------------------------------------------------------------------------------------------------
     *  @return ExecCommandError
     *  @throws DirectoryNotCreatedException
     *  @throws InvalidDirectoryParameterException
     *  ************************************************************************************************************* */
    public function createLogDirIfNotExists(): self
    {
        $this->fs->createDirectory(
            $this->logDir,
            0700,
            FileSystem::ATTR_DIR_CREATION_RECURSIVE
        );
        return $this;
    }
}
