<?php
/** *****************************************************************************************************************
 *  DumpMessage.php
 *  *****************************************************************************************************************
 *  @copyright 2022 Flagstone
 *  @author Emmanuel Grosdemange <emmanuel.grosdemange57@gmail.com>
 *  *****************************************************************************************************************
 *  Created: 2019/04/22
 *  ***************************************************************************************************************** */

namespace Flagstone\DatabaseDumpBundle\Service;

use Flagstone\DatabaseDumpBundle\Entity\DumpResponse;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Contracts\Translation\TranslatorInterface;

class DumpMessage
{
    private TranslatorInterface $translator;
    private string $locale;
    private RequestStack $request;

    const TRANSLATION_DOMAIN = 'dump-database';

    /** *************************************************************************************************************
     *  DumpMessage constructor.
     *  -------------------------------------------------------------------------------------------------------------
     *  @param TranslatorInterface $translator
     *  @param RequestStack $request
     *  ************************************************************************************************************* */
    public function __construct(TranslatorInterface $translator, RequestStack $request)
    {
        $this->translator = $translator;
        $this->request = $request;

        if ($this->request !== null) {
            if ($this->request->getCurrentRequest() !== null) {
                $this->locale = $this->request->getCurrentRequest()->getLocale();
            } else {
                $this->locale = 'en';
            }
        } else {
            $this->locale = 'en';
        }
    }

    /** *************************************************************************************************************
     *  Construction of the message returned after backups and restores
     *  -------------------------------------------------------------------------------------------------------------
     *  @param string $status
     *  @param string $message
     *  @param string $error
     *  @param string $filename
     *  @return DumpResponse
     *  ************************************************************************************************************* */
    public function get(string $status, string $message, string $error, string $filename = ''): DumpResponse
    {
        $dumpResponse = new DumpResponse(DumpResponse::RESPONSE_ERROR, '', '', '');

        $dumpResponse
            ->setStatus($status)
            ->setMessage(
                $this->translator->trans(
                    $message,
                    [],
                    self::TRANSLATION_DOMAIN,
                    $this->locale
                )
            )
            ->setError($error)
            ->setFilename($filename);

        return $dumpResponse;
    }
}