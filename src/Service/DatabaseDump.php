<?php
/** *****************************************************************************************************************
 *  DatabaseDump.php
 *  *****************************************************************************************************************
 *  @copyright 2022 Flagstone
 *  @author Emmanuel Grosdemange <emmanuel.grosdemange57@gmail.com>
 *  *****************************************************************************************************************
 *  Created: 2019/04/22
 *  ***************************************************************************************************************** */

namespace Flagstone\DatabaseDumpBundle\Service;

use Flagstone\ActionLoggingBundle\Entity\Log;
use Flagstone\ActionLoggingBundle\Service\Logging;
use Flagstone\DatabaseDumpBundle\Entity\DumpResponse;
use Flagstone\DatabaseDumpBundle\FlagstoneDatabaseDumpBundle;
use Symfony\Component\Finder\Finder;
use Symfony\Component\HttpKernel\KernelInterface;
use DateTime;
use Exception;
use Flagstone\DatabaseDumpBundle\Service\Exceptions\FileSystemExceptions\DirectoryNotCreatedException;
use Flagstone\DatabaseDumpBundle\Service\Exceptions\FileSystemExceptions\FileNotExistsException;
use Flagstone\DatabaseDumpBundle\Service\Exceptions\FileSystemExceptions\InvalidDirectoryParameterException;

/** *****************************************************************************************************************
 *  Class DatabaseDump
 *  -----------------------------------------------------------------------------------------------------------------
 *  All the necessary to dump and restore databases applications and uploaded documents
 *  Need some parameters on the .env file of the application, these parameters should't be stored on the database ;-)
 *  DATABASE_HOST : IP of the database location
 *  DATABASE_USER : Name of the user of the database
 *  DATABASE_PASSWORD : Password of the user of the database
 *  DATABASE_NAME : Name of the database
 *  These others parameters must be set in the services.yaml file, and not in database.
 *  UPLOAD_DIRS : Directories where the uploaded files are stored (array of directories, based on project_dir)
 *      key, value format :
 *          key : a name for the directory (without, special characters, space... only alphanumerics),
 *          value : the path of the directory
 *  DB_DUMP_DIRECTORY : Directory where database dump are stored (based on project_dir)
 *  MIGRATIONS_DIR : Directory where are stored Migrations database classes. Needed to dump works.
 *  -----------------------------------------------------------------------------------------------------------------
 *  @package Flagstone\DatabseDump\Service
 *  ***************************************************************************************************************** */
class DatabaseDump
{
    private KernelInterface $kernel;
    private CompressDump $compress;
    private DbDump $dbDump;
    private DbCreation $db;
    private array $uploadDirs;
    private string $dumpDir;
    private string $migrationsDir;
    private DumpMessage $dumpMessage;
    private Logging $logging;
    private Log $log;

    const DUMP_OK = 'ok';
    const DUMP_ERROR = 'error';
    const STR_ERROR_APP_MARKER_MESSAGE = 'database-dump.dump.error.app_marker';
    const STR_ERROR_FINAL_DUMP_MESSAGE = 'database-dump.dump.error.final';
    const STR_ERROR_UPLOADED_DUMP_MESSAGE = 'database-dump.dump.error.uploaded';
    const STR_ERROR_DB_DUMP_MESSAGE = 'database-dump.dump.error.database';
    const STR_ERROR_FINAL_RESTORE_MESSAGE = 'database-dump.restore.error.final';
    const STR_ERROR_UPLOADED_RESTORE_MESSAGE = 'database-dump.restore.error.uploaded';
    const STR_ERROR_DB_RESTORE_MESSAGE = 'database-dump.restore.error.database';
    const STR_ERROR_RESCUE_BACKUP_MESSAGE = 'database-dump.restore.error.rescue-backup';
    const STR_ERROR_MARKER_MESSAGE='database-dump.restore.error.marker_error';
    const STR_TEMPLATE_UPLOADS_FILE = 'dump_uploads_{key}_{migrationId}_{dumpDate}.tar.gz';
    const STR_TEMPLATE_RESCUE_UPLOADS_FILE = 'dump_uploads_{key}.tar.gz';
    const STR_FINAL_RESCUE_FILE='dump_rescue.tar.gz';
    const STR_DB_RESCUE_FILE='db_dump_rescue.sql.gz';

    /** *************************************************************************************************************
     *  DatabaseDump constructor.
     *  -------------------------------------------------------------------------------------------------------------
     *  @param KernelInterface $kernel
     *  @param CompressDump $compress
     *  @param DbDump $dbDump
     *  @param DbCreation $db
     *  @param DumpMessage $dumpMessage
     *  @param Logging $logging
     *  @param array $dumpInfos
     *  ************************************************************************************************************* */
    public function __construct(
        KernelInterface $kernel,
        CompressDump $compress,
        DbDump $dbDump,
        DbCreation $db,
        DumpMessage $dumpMessage,
        Logging $logging,
        array $dumpInfos
    ) {
        $this->dbDump = $dbDump;
        $this->db = $db;
        $this->compress = $compress;
        $this->kernel = $kernel;
        $this->dumpMessage = $dumpMessage;
        $this->uploadDirs = $dumpInfos['upload_dirs'];
        $this->dumpDir = $dumpInfos['dump_dir'];
        $this->migrationsDir = $dumpInfos['migrations_dir'];
        $this->logging = $logging;
        $this->log = new Log();
        $this->log->setContext(FlagstoneDatabaseDumpBundle::getContext());
    }

    /** *************************************************************************************************************
     *  Make the dump of the database and the uploaded files.
     *  There is also a rescue mode. This is used when a restoration is made to revert the process if an error occured.
     *  All files are compressed.
     *  -------------------------------------------------------------------------------------------------------------
     *  @param  bool        $tempBackup
     *  @param  string|null $finalDumpDir
     *  @return null|DumpResponse
     *  @throws Exception
     *  ************************************************************************************************************* */
    public function make(bool $tempBackup = false, ?string &$finalDumpDir = null): DumpResponse
    {
        $this->log->setAction('make');
        $this->logging->info($this->log, 'Begin Make dump');
        $tmpDir = $this->createTmpDir();
        if (true === $tempBackup) {
            $finalDumpDir = $this->createTmpDir();
            $dbFilename = self::STR_DB_RESCUE_FILE;
            $templateUploadsDumpFiles = self::STR_TEMPLATE_RESCUE_UPLOADS_FILE;
            $finalFilename = self::STR_FINAL_RESCUE_FILE;
        } else {
            $migrationId = $this->getLastMigrationDate();
            $dumpDate = date('YmdHis');
            $dbFilename = 'db_dump_'.$migrationId.'_'.$dumpDate.'.sql.gz';
            $finalDumpDir = $this->getDumpDir();
            $templateUploadsDumpFiles = str_replace('{migrationId}', $migrationId, self::STR_TEMPLATE_UPLOADS_FILE);
            $templateUploadsDumpFiles = str_replace('{dumpDate}', $dumpDate, $templateUploadsDumpFiles);
            $finalFilename = 'dump_'.$migrationId.'_'.$dumpDate.'.tar.gz';
        }
        $fullDbFilename = $tmpDir.'/'.$dbFilename;

        $backupDb = $this->dbDump->backupDB($fullDbFilename);
        if ('' === $backupDb) {
            $uploadsMade = $this->compress->compressUploads(
                $tmpDir,
                $templateUploadsDumpFiles,
                $this->kernel->getProjectDir(),
                $this->uploadDirs,
                $uploadsDump,
                $uploadsFileArray
            );
            if (true === $uploadsMade) {
                $fullFilename = $finalDumpDir.'/'.$finalFilename;
                $dumpResponse = $this->makeFinalDump($fullFilename, $tmpDir, $finalFilename, $fullDbFilename, $uploadsFileArray);
            } else {
                //  Uploaded files dump failed
                $this->rollbackDumps($fullDbFilename, $uploadsFileArray);
                $dumpResponse = $this->dumpMessage->get(
                    DumpResponse::RESPONSE_ERROR,
                    self::STR_ERROR_UPLOADED_DUMP_MESSAGE,
                    $uploadsDump
                );
            }
        } else {
            //  Database dump failed
            $this->removeDump($fullDbFilename);
            $dumpResponse = $this->dumpMessage->get(
                DumpResponse::RESPONSE_ERROR,
                self::STR_ERROR_DB_DUMP_MESSAGE,
                $backupDb
            );
        }
        return $dumpResponse;
    }

    /** *************************************************************************************************************
     *  Make the restoration of the database and the uploaded files (with rescue mode if necessary, view make method).
     *  -------------------------------------------------------------------------------------------------------------
     *  @param  string      $dumpFile
     *  @param  bool        $rescue
     *  @param  string|null $tmpRescueDir
     *  @return DumpResponse
     *  @throws DirectoryNotCreatedException
     *  @throws FileNotExistsException
     *  @throws InvalidDirectoryParameterException
     *  @throws Exception
     *  ************************************************************************************************************* */
    public function restore(
        string $dumpFile,
        bool $rescue = false,
        ?string $tmpRescueDir = null): DumpResponse
    {
        $tmpDir = $this->createTmpDir();
        $dumpDirectory = $this->getDumpDir();

        /*
         * Make a rescue backup before processing the restoration.
         * If restoration failed at any time, just make a restoration of the rescue backup.
         */
        $rescueBackup = null;
        if (false === $rescue) {
            $rescueBackup = $this->make(true, $finalTmpDir);
        } else {
            $dumpDirectory = $tmpRescueDir;
        }

        if (null === $rescueBackup || 'ok' === $rescueBackup->getStatus()) {
            if (false !== $this->compress->testAppMarker($dumpDirectory.$dumpFile)) {
                $this->compress->removeAppMarker($dumpDirectory, $dumpFile, $tmpDir);
                $restoreDump = $this->compress->uncompress($tmpDir.'/', $dumpFile, $tmpDir);

                if ('' === $restoreDump) {
                    $response = $this->doRestore($dumpFile, $rescue, $tmpRescueDir, $tmpDir);
                } else {
                    $response = $this->dumpMessage->get(
                        DumpResponse::RESPONSE_ERROR,
                        self::STR_ERROR_FINAL_RESTORE_MESSAGE,
                        $restoreDump,
                        $dumpFile
                    );
                    $this->restoreIfNotRescueMode($rescue, $tmpRescueDir);
                }
            } else {
                $response = $this->dumpMessage->get(
                    DumpResponse::RESPONSE_ERROR,
                    self::STR_ERROR_MARKER_MESSAGE,
                    '',
                    $dumpFile
                );
            }
        } else {
            $response = $this->dumpMessage->get(
                DumpResponse::RESPONSE_ERROR,
                self::STR_ERROR_RESCUE_BACKUP_MESSAGE,
                $rescueBackup->getError(),
                $dumpFile
            );
        }

        return $response;
    }

    /** *************************************************************************************************************
     *  @param  bool    $rescue
     *  @param  string  $tmpRescueDir
     *  @throws DirectoryNotCreatedException
     *  @throws FileNotExistsException
     *  @throws InvalidDirectoryParameterException
     */
    private function restoreIfNotRescueMode(bool $rescue, string $tmpRescueDir): void
    {
        if (false === $rescue) {
            $this->restore(self::STR_FINAL_RESCUE_FILE, true, $tmpRescueDir);
        }
    }

    /** *************************************************************************************************************
     *  Remove all dump file where dump failed.
     *  -------------------------------------------------------------------------------------------------------------
     *  @param  $fullDbFilename
     *  @param  $uploadsFileArray
     *  @throws Exception
     *  ************************************************************************************************************* */
    private function rollbackDumps($fullDbFilename, $uploadsFileArray)
    {
        $this->removeDump($fullDbFilename);
        foreach($uploadsFileArray as $uploadsFile) {
            $this->removeDump($uploadsFile);
        }
    }

    /** *************************************************************************************************************
     *  Return the full dump directory
     *  -------------------------------------------------------------------------------------------------------------
     *  @return string
     *  ************************************************************************************************************* */
    public function getDumpDir(): string
    {
        return $this->dumpDir;
    }

    /** *************************************************************************************************************
     *  Get list of available dumps.
     *  -------------------------------------------------------------------------------------------------------------
     *  @return Finder|null
     *  ************************************************************************************************************* */
    public function getDumps(): ?Finder
    {
        if (file_exists($this->getDumpDir())) {
            $finder = new Finder();
            $finder
                ->files()
                ->in($this->getDumpDir())
                ->name('dump*.tar.gz')
                ->sortByName()
                ->reverseSorting();

            if ($finder->hasResults()) {
                return $finder;
            }
        }

        return null;
    }

    /** *************************************************************************************************************
     *  Create the tmp directory where will be stored the dumped files before final storage
     *  Return the tmpDir path (without the dump path)
     *  -------------------------------------------------------------------------------------------------------------
     *  @return string
     *  @throws Exception
     *  ************************************************************************************************************* */
    private function createTmpDir(): string
    {
        $tmpDir = tempnam(sys_get_temp_dir(), '');
        if (file_exists($tmpDir)) {
            unlink($tmpDir);
            mkdir($tmpDir);
            $this->log->setAction('createTmpDir');
            $this->logging->info($this->log, 'Create Temporary Directory : '.$tmpDir);
        }
        return $tmpDir;
    }

    /** *************************************************************************************************************
     *  Remove a dump file
     *  -------------------------------------------------------------------------------------------------------------
     *  @param  $dumpFile
     *  @throws Exception
     *  ************************************************************************************************************* */
    private function removeDump($dumpFile)
    {
        if (file_exists($dumpFile)) {
            $this->log->setAction('removeDump');
            $this->logging->info($this->log, 'Remove Backup file : '.$dumpFile);
            unlink($dumpFile);
        }
    }

    /** *************************************************************************************************************
     *  Search in the Migrations application directory the last migration date Id.
     *  Return the Id if find, else false.
     *  -------------------------------------------------------------------------------------------------------------
     *  @return bool|string
     *  ************************************************************************************************************* */
    private function getLastMigrationDate()
    {
        $migrationDirectory = $this->kernel->getProjectDir().$this->migrationsDir;
        $migrations = new Finder();
        $migrations
            ->files()
            ->in($migrationDirectory)
            ->name('*.php')
            ->notName('.*');
        $migrations
            ->sortByName()
            ->reverseSorting();

        if ($migrations->hasResults()) {
            $pattern = '/Version(.*)\.php/';

            foreach ($migrations as $migration) {
                if (1 === preg_match($pattern, $migration->getFilename(), $filename)) {
                    return $filename[1];
                }
            }
        }
        return false;
    }

    /** *************************************************************************************************************
     *  Return the file date creation, based on the filename string
     *  -------------------------------------------------------------------------------------------------------------
     *  @param  string  $filename
     *  @return DateTime
     *  @throws Exception
     *  ************************************************************************************************************* */
    public function getCreationDate(string $filename): DateTime
    {
        $pattern = '/dump_\d*_(\d*)\.tar\.gz/';
        preg_match($pattern, $filename, $arrayDump);

        return $this->getDate($arrayDump[1]);
    }

    /** *************************************************************************************************************
     *  Return the migration file date, based on the filename string
     *  -------------------------------------------------------------------------------------------------------------
     *  @param  string  $filename
     *  @param  string  $stringDate
     *  @return DateTime
     *  @throws Exception
     *  ************************************************************************************************************* */
    public function getMigrationDate(string $filename, ?string &$stringDate = null): DateTime
    {
        $pattern = '/dump_(\d*)_\d*\.tar\.gz/';
        preg_match($pattern, $filename, $arrayDump);

        $stringDate = $arrayDump[1];
        return $this->getDate($arrayDump[1]);
    }

    /** *************************************************************************************************************
     *  Return a date Object, based on a date-time string (YMDHiS format).
     *  -------------------------------------------------------------------------------------------------------------
     *  @param  string  $timestamp
     *  @return DateTime
     *  @throws Exception
     *  ************************************************************************************************************* */
    private function getDate(string $timestamp): DateTime
    {
        $patternDate = '/(\d{4})(\d{2})(\d{2})(\d{2})(\d{2})(\d{2})/';
        preg_match($patternDate, $timestamp, $arrayDate);

        $dateDump = new DateTime();
        $dateDump->setDate($arrayDate[1], $arrayDate[2], $arrayDate[3]);

        return $dateDump->setTime($arrayDate[4], $arrayDate[5], $arrayDate[6]);
    }

    /** *************************************************************************************************************
     *  Calculate an age level to determinate how old is a dump.
     *  -------------------------------------------------------------------------------------------------------------
     *  @param  string  $filename
     *  @return int
     *  @throws Exception
     *  ************************************************************************************************************* */
    public function getAge(string $filename): int
    {
        $age = 0;
        $lastMigrationId = $this->getLastMigrationDate();

        $this->getMigrationDate($filename, $stringMigrationDate);
        if ($stringMigrationDate !== $lastMigrationId) {
            $age += 50;
        }

        $creationDate = $this->getCreationDate($filename);
        $currentDate = new DateTime();

        $dateDiff = $currentDate->diff($creationDate, true);
        $age += $dateDiff->d;

        $ageLevel = 3;
        if ($age < 7) {
            $ageLevel = 0;
        } elseif ($age < 50) {
            $ageLevel = 1;
        } elseif ($age < 100) {
            $ageLevel = 2;
        }

        return $ageLevel;
    }

    /** *************************************************************************************************************
     *  Operations to do when a dump failed at its end
     *  -   rollback uploads and database dump
     *  -   remove dump file
     *  -------------------------------------------------------------------------------------------------------------
     *  @param  string  $fullDbFilename
     *  @param  array   $uploadsFileArray
     *  @param  string  $fullFilename
     *  @param  string  $errorMessage
     *  @return DumpResponse
     *  ************************************************************************************************************* */
    private function finalDumpFailed(
        string $fullDbFilename,
        array $uploadsFileArray,
        string $fullFilename,
        string $errorMessage): DumpResponse
    {
        $this->rollbackDumps($fullDbFilename, $uploadsFileArray);
        $this->removeDump($fullFilename);

        return $this->dumpMessage->get(
            DumpResponse::RESPONSE_ERROR,
            self::STR_ERROR_FINAL_DUMP_MESSAGE,
            $errorMessage
        );
    }

    /** *************************************************************************************************************
     *  Make the final dump, take all dumped files and make an unique dump file
     *  -------------------------------------------------------------------------------------------------------------
     *  @param string $fullFilename
     *  @param string $tmpDir
     *  @param string $finalFilename
     *  @param string $fullDbFilename
     *  @param array $uploadsFileArray
     *  @return DumpResponse
     *  @throws DirectoryNotCreatedException
     *  @throws FileNotExistsException
     *  @throws InvalidDirectoryParameterException
     *  ************************************************************************************************************* */
    private function makeFinalDump(
        string $fullFilename,
        string $tmpDir,
        string $finalFilename,
        string $fullDbFilename,
        array $uploadsFileArray): DumpResponse
    {
        $finalDump = $this->compress->compress($fullFilename, $tmpDir);

        if ('' === $finalDump) {
            if (false !== $this->compress->addAppMarker($fullFilename)) {
                chmod($fullFilename, 0660);
                $dumpResponse = $this->dumpMessage->get(
                    DumpResponse::RESPONSE_OK,
                    '',
                    '',
                    $finalFilename
                );
            } else {
                unlink($fullFilename);
                $dumpResponse = $this->finalDumpFailed(
                    $fullDbFilename,
                    $uploadsFileArray,
                    $fullFilename,
                    self::STR_ERROR_APP_MARKER_MESSAGE
                );
            }
        } else {
            //  Final dump failed
            $dumpResponse = $this->finalDumpFailed(
                $fullDbFilename,
                $uploadsFileArray,
                $fullFilename,
                $finalDump
            );
        }

        $this->removeTmpDirectory($tmpDir);
        return $dumpResponse;
    }

    /** *************************************************************************************************************
     *  @param  string      $dumpFile
     *  @param  bool        $rescue
     *  @param  string|null $tmpRescueDir
     *  @param  string      $tmpDir
     *  @return DumpResponse
     *  @throws DirectoryNotCreatedException
     *  @throws FileNotExistsException
     *  @throws InvalidDirectoryParameterException
     *  ************************************************************************************************************* */
    private function doRestore(string $dumpFile, bool $rescue, ?string $tmpRescueDir, string $tmpDir): DumpResponse
    {
        $this->log->setAction('doRestore');
        $this->logging->info($this->log, 'Begin backup restoration');

        $dbDumpFile = str_replace('dump_', 'db_dump_', $dumpFile);
        /*
         * Create database manually.
         */
        $this->db->createDatabase();
        $restoreDb = $this->dbDump->restoreDb($tmpDir, $dbDumpFile);
        if ('' === $restoreDb) {
            $restoreUploads = $this->compress->uncompressUploads(
                $tmpDir . '/',
                $this->uploadDirs,
                $this->kernel->getProjectDir(),
                $dumpFile,
                $restorationMessage
            );
            if (true === $restoreUploads) {
                $response = $this->dumpMessage->get(
                    DumpResponse::RESPONSE_OK,
                    '',
                    '',
                    $dumpFile
                );
            } else {
                $response = $this->dumpMessage->get(
                    DumpResponse::RESPONSE_ERROR,
                    self::STR_ERROR_UPLOADED_RESTORE_MESSAGE,
                    $restorationMessage,
                    $dumpFile,
                );
                $this->restoreIfNotRescueMode($rescue, $tmpRescueDir);
            }
        } else {
            $response = $this->dumpMessage->get(
                DumpResponse::RESPONSE_ERROR,
                self::STR_ERROR_DB_RESTORE_MESSAGE,
                $restoreDb,
                $dumpFile
            );
            $this->restoreIfNotRescueMode($rescue, $tmpRescueDir);
        }

        return $response;
    }

    /**
     * @param   string $tmpDir
     * @return  bool
     * @throws  Exception
     */
    private function removeTmpDirectory(string $tmpDir): bool
    {
        if (!file_exists($tmpDir)) {
            return true;
        }

        if (!is_dir($tmpDir)) {
            return unlink($tmpDir);
        }

        foreach(scandir($tmpDir) as $item) {
            if ('.' === $item || '..' === $item) {
                continue;
            }
            if (!$this->removeTmpDirectory($tmpDir . DIRECTORY_SEPARATOR . $item)) {
                return false;
            }
        }

        $this->log->setAction('removeTmpDirectory');
        $this->logging->info($this->log, 'Remove temporary directory : '.$tmpDir);
        return rmdir($tmpDir);
    }
}
