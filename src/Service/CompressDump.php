<?php
/** *****************************************************************************************************************
 *  DatabaseDumpBundle.php
 *  *****************************************************************************************************************
 *  @copyright 2022 Flagstone
 *  @author Emmanuel Grosdemange <emmanuel.grosdemange57@gmail.com>
 *  *****************************************************************************************************************
 *  Created: 2022/04/22
 *  ***************************************************************************************************************** */

namespace Flagstone\DatabaseDumpBundle\Service;

use Exception;
use Flagstone\ActionLoggingBundle\Entity\Log;
use Flagstone\ActionLoggingBundle\Entity\LogEntityInterface;
use Flagstone\ActionLoggingBundle\Service\Logging;
use Monolog\Logger;
use Flagstone\DatabaseDumpBundle\Service\Exceptions\FileSystemExceptions\DirectoryNotCreatedException;
use Flagstone\DatabaseDumpBundle\Service\Exceptions\FileSystemExceptions\InvalidDirectoryParameterException;
use Flagstone\DatabaseDumpBundle\Service\Exceptions\FileSystemExceptions\FileNotExistsException;

/** *****************************************************************************************************************
 *  Class CompressDump
 *  -----------------------------------------------------------------------------------------------------------------
 *  Compress and uncompress files
 *  -----------------------------------------------------------------------------------------------------------------
 *  @package Flagstone\DatabseDump\Service
 *  ***************************************************************************************************************** */
class CompressDump
{
    /**
     * @var Logging
     */
    private Logging $logger;
    /**
     * @var ExecCommandError
     */
    private ExecCommandError $error;
    /**
     * @var string
     */
    private string $appMarker;
    private LogEntityInterface $log;

    /** *************************************************************************************************************
     *  CompressDump constructor.
     *  -------------------------------------------------------------------------------------------------------------
     *  @param Logging $logger
     *  @param ExecCommandError $error
     *  @param string $appMarker
     *  ************************************************************************************************************* */
    public function __construct(Logging $logger, ExecCommandError $error, string $appMarker)
    {
        $this->logger = $logger;
        $this->error = $error;
        $this->appMarker = $appMarker;
        $this->log = new Log();
        $this->log
            ->setContext('DatabaseDumpBundle');
    }

    /** *************************************************************************************************************
     *  Make compression for uploaded files, with multiple directories. All failed if one directory compression
     *  fails.
     *  -------------------------------------------------------------------------------------------------------------
     *  @param string $tmpDir
     *  @param string $fileTemplateName
     *  @param string $projectDir
     *  @param array $uploadDirs
     *  @param string|null $uploadsDump
     *  @param array|null $uploadsFileArray
     *  @return bool
     *  @throws Exception
     *  ************************************************************************************************************* */
    public function compressUploads(
        string $tmpDir,
        string $fileTemplateName,
        string $projectDir,
        array $uploadDirs,
        ?string &$uploadsDump,
        ?array &$uploadsFileArray): bool
    {
        $uploadsMade = true;

        $uploadsFileArray = [];
        foreach ($uploadDirs as $key => $uploadsDir) {
            $uploadsFilename = str_replace('{key}', $key, $fileTemplateName);
            $fullUploadsFilename = $tmpDir.'/'.$uploadsFilename;
            $fullUploadsDir = $projectDir.$uploadsDir;

            $uploadsFileArray[] = $fullUploadsFilename;
            $uploadsDump = $this->compress($fullUploadsFilename, $fullUploadsDir);
            if ('' !== $uploadsDump) {
                $uploadsMade = false;
                break;
            }
        }
        return $uploadsMade;
    }

    /** *************************************************************************************************************
     *  Uncompression for uploaded files, with multiple directories. All failed if one directory uncompression
     *  fails.
     *  -------------------------------------------------------------------------------------------------------------
     *  @param string $dumpDir
     *  @param array $uploadDirs
     *  @param string $projectDir
     *  @param string $dumpFilename
     *  @param string|null $restorationMessage
     *  @return bool
     *  @throws DirectoryNotCreatedException
     *  @throws FileNotExistsException
     *  @throws InvalidDirectoryParameterException
     *  ************************************************************************************************************* */
    public function uncompressUploads(
        string $dumpDir,
        array $uploadDirs,
        string $projectDir,
        string $dumpFilename,
        ?string &$restorationMessage): bool
    {
        $restoreMade = true;

        foreach ($uploadDirs as $key => $uploadsDir) {
            $fileToUncompress = str_replace('dump', 'dump_uploads_'.$key, $dumpFilename);
            $fullUploadsDir = $projectDir.$uploadsDir;
            $restorationMessage = $this->uncompress($dumpDir, $fileToUncompress, $fullUploadsDir);
            if ('' !== $restorationMessage) {
                $restoreMade = false;
                break;
            }
        }

        return $restoreMade;
    }

    /** *************************************************************************************************************
     *  Compress some files given in array to a .tar.gz file
     *  Return true if no error else false
     *  -------------------------------------------------------------------------------------------------------------
     *  @param string $filename
     *  @param string $files
     *  @return string Return an empty string if ok, else, the message returned by the command
     *  @throws DirectoryNotCreatedException
     *  @throws FileNotExistsException
     *  @throws InvalidDirectoryParameterException
     *  @throws Exception
     *  ************************************************************************************************************* */
    public function compress(string $filename, string $files): string
    {
        $errorFilename = $this->error
            ->createLogDirIfNotExists()
            ->setErrorFilename()
            ->getFullErrorFilename();

        $execCompress =
            'tar -czf '.
            $filename.
            ' -C '.$files.
            ' . 2> '.$errorFilename;

        $this->log->setAction('compressDump');
        $this->logger->dump($this->log, '[CompressDump::compress] : ' . $execCompress);
        exec($execCompress);

        return $this->error->get();
    }

    /** *************************************************************************************************************
     *  Uncompress a .tar.gz file in a tmp directory given in parameter
     *  Return true if no error else false
     *  -------------------------------------------------------------------------------------------------------------
     *  @param string $dumpDir
     *  @param string $dumpFile
     *  @param string $destinationDir
     *  @return string Return an empty string if ok, else, the message returned by the command
     *  @throws DirectoryNotCreatedException
     *  @throws FileNotExistsException
     *  @throws InvalidDirectoryParameterException
     *  @throws Exception
     *  ************************************************************************************************************* */
    public function uncompress(string $dumpDir, string $dumpFile, string $destinationDir): string
    {
        $errorFilename = $this->error
            ->createLogDirIfNotExists()
            ->setErrorFilename()
            ->getFullErrorFilename();

        $execUncompress =
            'tar -xzf '.
            $dumpDir.$dumpFile.
            ' -C '.$destinationDir.
            ' . 2> '.$errorFilename;

        $this->log->setAction('uncompressDump');
        $this->logger->dump($this->log, '[CompressDump::uncompress] : ' . $execUncompress);
        exec($execUncompress);

        return $this->error->get();
    }

    /** *************************************************************************************************************
     *  Add a marker to the dump file to mark the application, to prevent false restoration.
     *  -------------------------------------------------------------------------------------------------------------
     *  @param string $filename
     *  @return int
     *  ************************************************************************************************************* */
    public function addAppMarker(string $filename): int
    {
        $tmpFile = $filename.'.tmp';
        $fOut = fopen($tmpFile, 'w');
        fwrite($fOut, $this->appMarker);
        fclose($fOut);
        $this->chunkedCopy($filename, $tmpFile);

        return $this->chunkedCopy($tmpFile, $filename, 'w');
    }

    /** *************************************************************************************************************
     *  Remove the marker of the file and save the file in the tmpdir.
     *  -------------------------------------------------------------------------------------------------------------
     *  @param string $dumpDir
     *  @param string $filename
     *  @param string $tmpDir
     *  @return false|int
     *  ************************************************************************************************************* */
    public function removeAppMarker(string $dumpDir, string $filename, string $tmpDir)
    {
        $tmpFile = $tmpDir.'/'.$filename;
        $bufferSize = 1024 * 1024;
        $sizeCount = 0;
        $initBlock = 1;
        $fIn = fopen($dumpDir.$filename, 'rb');
        $fOut = fopen($tmpFile, 'w');
        while(!feof($fIn)) {
            $block = fread($fIn, $bufferSize);
            if (1 === $initBlock) {
                $block = str_replace($this->appMarker, '', $block);
            }
            $sizeCount += fwrite($fOut, $block);
        }
        fclose($fIn);
        fclose($fOut);

        return $sizeCount;
    }

    /** *************************************************************************************************************
     *  Make a copy foor large files
     *  -------------------------------------------------------------------------------------------------------------
     *  @param $from
     *  @param $to
     *  @param string $writeMode
     *  @return false|int
     *  ************************************************************************************************************* */
    private function chunkedCopy($from, $to, $writeMode = 'a')
    {
        $bufferSize = 1024 * 1024;
        $sizeCount = 0;
        $fIn = fopen($from, 'rb');
        $fOut = fopen($to, $writeMode);
        while(!feof($fIn)) {
            $sizeCount += fwrite($fOut, fread($fIn, $bufferSize));
        }
        fclose($fIn);
        fclose($fOut);
        return $sizeCount;
    }

    /** *************************************************************************************************************
     *  Test if the marker of the file is the good one.
     *  -------------------------------------------------------------------------------------------------------------
     *  @param string $filename
     *  @return bool
     *  ************************************************************************************************************* */
    public function testAppMarker(string $filename)
    {
        /*  Take the first 1024 bytes of the file to get the App Marker */
        $fIn = fopen($filename, 'rb');
        $fileContent = fread($fIn, 1024);
        fclose($fIn);
        $fileMarker = substr_compare($fileContent, $this->appMarker, 0, strlen($this->appMarker));

        if (0 === $fileMarker) {
            return true;
        }
        return false;
    }
}