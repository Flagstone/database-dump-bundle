<?php
/** *****************************************************************************************************************
 *  DumpResponse.php
 *  *****************************************************************************************************************
 *  @copyright 2022 Flagstone
 *  @author Emmanuel Grosdemange <emmanuel.grosdemange57@gmail.com>
 *  *****************************************************************************************************************
 *  Created: 2022/04/22
 *  ***************************************************************************************************************** */

namespace Flagstone\DatabaseDumpBundle\Entity;

/** *****************************************************************************************************************
 *  Class DumpResponse
 *  @package Flagstone\DatabaseDumpBundle\Entity
 *  ***************************************************************************************************************** */
class DumpResponse
{
    /**
     * @var string
     */
    private $status;
    /**
     * @var string
     */
    private $message;
    /**
     * @var string
     */
    private $error;
    /**
     * @var string
     */
    private $filename;

    const RESPONSE_OK = 'ok';
    const RESPONSE_ERROR = 'error';

    /** *************************************************************************************************************
     *  DumpResponse constructor.
     *  -------------------------------------------------------------------------------------------------------------
     *  @param string $status
     *  @param string $filename
     *  @param string $message
     *  @param string $error
     *  ************************************************************************************************************* */
    public function __construct(string $status, string $filename, string $message, string $error)
    {
        $this->setStatus($status);
        $this->setFilename($filename);
        $this->setMessage($message);
        $this->setError($error);
    }

    /**
     * @param string $status
     * @return $this
     */
    public function setStatus(string $status): self
    {
        $this->status = $status;
        return $this;
    }

    /**
     * @param null|string $filename
     * @return $this
     */
    public function setFilename(?string $filename): self
    {
        $this->filename = $filename;
        return $this;
    }

    /**
     * @param null|string $message
     * @return $this
     */
    public function setMessage(?string $message): self
    {
        $this->message = $message;
        return $this;
    }

    /**
     * @param null|string $error
     * @return $this
     */
    public function setError(?string $error): self
    {
        $this->error = $error;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getStatus(): ?string
    {
        return $this->status;
    }

    /**
     * @return string|null
     */
    public function getFilename(): ?string
    {
        return $this->filename;
    }

    /**
     * @return string|null
     */
    public function getMessage(): ?string
    {
        return $this->message;
    }

    /**
     * @return string|null
     */
    public function getError(): ?string
    {
        return $this->error;
    }

    /**
     * @return array
     */
    public function getArray(): array
    {
        return [
            'status'    => $this->getStatus(),
            'filename'  => $this->getFilename(),
            'message'   => $this->getMessage(),
            'error'     => $this->getError()
        ];
    }
}