<?php
/** *****************************************************************************************************************
 *  CreateAdminUser.php
 *  *****************************************************************************************************************
 *  @copyright 2019 Flagstone
 *  @author Emmanuel Grosdemange <emmanuel.grosdemange57@gmail.com>
 *  *****************************************************************************************************************
 *  Created: 2022/04/22
 *  ***************************************************************************************************************** */

namespace Flagstone\DatabaseDumpBundle\Command;

use Flagstone\DatabaseDumpBundle\Entity\DumpResponse;
use Flagstone\DatabaseDumpBundle\Service\DatabaseDump;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Exception;

class DatabaseDumpMakeCommand extends Command
{
    /**
     * @var string
     */
    protected static $defaultName = 'fs-database-dump:make';

    /**
     * @var DatabaseDump
     */
    protected $dump;

    /** ************************************************************************************************************
     *  CreateAdminUser constructor.
     *  ------------------------------------------------------------------------------------------------------------
     *  @param DatabaseDump $dump
     *  ************************************************************************************************************* */
    public function __construct(DatabaseDump $dump)
    {
        parent::__construct();
        $this->dump = $dump;
    }

    /** *************************************************************************************************************
     *  Configure the command
     *  ************************************************************************************************************* */
    protected function configure()
    {
        $this
            ->setDescription('Make a backup of the database and uploaded folders.');
    }

    /** *************************************************************************************************************
     *  @param InputInterface $input
     *  @param OutputInterface $output
     *  @return int|void|null
     *  @throws Exception
     *  ************************************************************************************************************* */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln([
            '',
            '<fg=green>Flagstone Database Dump : Make a backup of database and uploaded files</>',
            '<fg=yellow>=====================================================================</>',
            ''
        ]);

        /**
         * @var DumpResponse
         */
        $response = $this->dump->make();

        if ($response->getStatus() === $response::RESPONSE_OK) {
            $output->writeln([
                '',
                '<fg=green>Backup maded</>',
                '<fg=white>File: '.$response->getFilename().'</>',
                '<fg=yellow>============</>',
                ''
            ]);
        } else {
            $output->writeln([
                '',
                '<fg=red>Backup failed</>',
                '<fg=red>'.$response->getMessage().'</>',
                '<fg=red>--------------</>',
                '<fg=red>'.str_ireplace(['<br />', '</br>', '<br />', '<br>'], "\n", $response->getError()).'</>',
                '<fg=yellow>============</>',
                ''
            ]);
        }
    }
}
