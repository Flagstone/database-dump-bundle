<?php

namespace Flagstone\DatabaseDumpBundle\Twig;

use Symfony\Component\DependencyInjection\ParameterBag\ContainerBag;
use Symfony\Component\Finder\Finder;
use Symfony\Component\HttpKernel\Kernel;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;
use DateTime;

class DatabaseDumpExtension extends AbstractExtension
{
    private Kernel $kernel;
    private ContainerBag $parameterBag;

    public function __construct(Kernel $kernel, ContainerBag $parameterBag)
    {
        $this->kernel = $kernel;
        $this->parameterBag = $parameterBag;
    }

    public function getFunctions()
    {
        return [
            new TwigFunction('getAge', [$this, 'getAge']),
            new TwigFunction('getDate', [$this, 'getDate']),
            new TwigFunction('getCreationDate', [$this, 'getCreationDate']),
            new TwigFunction('getMigrationDate', [$this, 'getMigrationDate']),
        ];
    }

    /** *************************************************************************************************************
     *  Calculate an age level to determinate how old is a dump.
     *  -------------------------------------------------------------------------------------------------------------
     *  @param  string  $filename
     *  @return int
     *  @throws Exception
     *  ************************************************************************************************************* */
    public function getAge(string $filename): int
    {
        $age = 0;
        $lastMigrationId = $this->getLastMigrationDate();

        $this->getMigrationDate($filename, $stringMigrationDate);
        if ($stringMigrationDate !== $lastMigrationId) {
            $age += 50;
        }

        $creationDate = $this->getCreationDate($filename);
        $currentDate = new DateTime();

        $dateDiff = $currentDate->diff($creationDate, true);
        $age += $dateDiff->d;

        $ageLevel = 3;
        if ($age < 7) {
            $ageLevel = 0;
        } elseif ($age < 50) {
            $ageLevel = 1;
        } elseif ($age < 100) {
            $ageLevel = 2;
        }

        return $ageLevel;
    }

    /** *************************************************************************************************************
     *  Return a date Object, based on a date-time string (YMDHiS format).
     *  -------------------------------------------------------------------------------------------------------------
     *  @param  string  $timestamp
     *  @return DateTime
     *  @throws Exception
     *  ************************************************************************************************************* */
    private function getDate(string $timestamp): DateTime
    {
        $patternDate = '/(\d{4})(\d{2})(\d{2})(\d{2})(\d{2})(\d{2})/';
        preg_match($patternDate, $timestamp, $arrayDate);

        $dateDump = new DateTime();
        $dateDump->setDate($arrayDate[1], $arrayDate[2], $arrayDate[3]);

        return $dateDump->setTime($arrayDate[4], $arrayDate[5], $arrayDate[6]);
    }

    /** *************************************************************************************************************
     *  Return the file date creation, based on the filename string
     *  -------------------------------------------------------------------------------------------------------------
     *  @param  string  $filename
     *  @return DateTime
     *  @throws Exception
     *  ************************************************************************************************************* */
    public function getCreationDate(string $filename): DateTime
    {
        $pattern = '/dump_\d*_(\d*)\.tar\.gz/';
        preg_match($pattern, $filename, $arrayDump);

        return $this->getDate($arrayDump[1]);
    }

    /** *************************************************************************************************************
     *  Return the migration file date, based on the filename string
     *  -------------------------------------------------------------------------------------------------------------
     *  @param  string  $filename
     *  @param  string  $stringDate
     *  @return DateTime
     *  @throws Exception
     *  ************************************************************************************************************* */
    public function getMigrationDate(string $filename, ?string &$stringDate = null): DateTime
    {
        $pattern = '/dump_(\d*)_\d*\.tar\.gz/';
        preg_match($pattern, $filename, $arrayDump);

        $stringDate = $arrayDump[1];
        return $this->getDate($arrayDump[1]);
    }

    /** *************************************************************************************************************
     *  Search in the Migrations application directory the last migration date Id.
     *  Return the Id if find, else false.
     *  -------------------------------------------------------------------------------------------------------------
     *  @return bool|string
     *  ************************************************************************************************************* */
    private function getLastMigrationDate()
    {
        $migrationDirectory = $this->kernel->getProjectDir().$this->parameterBag->get('database_dump_migrationsDir');
        $migrations = new Finder();
        $migrations
            ->files()
            ->in($migrationDirectory)
            ->name('*.php')
            ->notName('.*');
        $migrations
            ->sortByName()
            ->reverseSorting();

        if ($migrations->hasResults()) {
            $pattern = '/Version(.*)\.php/';

            foreach ($migrations as $migration) {
                if (1 === preg_match($pattern, $migration->getFilename(), $filename)) {
                    return $filename[1];
                }
            }
        }
        return false;
    }
}