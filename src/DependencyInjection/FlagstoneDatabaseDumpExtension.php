<?php
/** ******************************************************************************************************************
 *  FlagstoneDatabaseDumpExtension.php
 *  ******************************************************************************************************************
 *  @author: Emmanuel Grosdemange <emmanuel.grosdemange57@gmail.com>
 *  @Copyright 2022 Flagstone
 *  *****************************************************************************************************************
 *  Created: 2022/04/22
 *  ***************************************************************************************************************** */

namespace Flagstone\DatabaseDumpBundle\DependencyInjection;

use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Extension\Extension;
use Symfony\Component\DependencyInjection\Loader\YamlFileLoader;
use Exception;

class FlagstoneDatabaseDumpExtension extends Extension
{
    /** ************************************************************************************************************
     *  @param array $configs
     *  @param ContainerBuilder $container
     *  @throws Exception
     *  ************************************************************************************************************* */
    public function load(array $configs, ContainerBuilder $container)
    {
        $loader = new YamlFileLoader(
            $container,
            new FileLocator(__DIR__.'/../Resources/config')
        );
        $loader->load('services.yaml');
    }
}