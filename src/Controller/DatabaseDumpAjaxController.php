<?php
/** *****************************************************************************************************************
 *  DatabaseDumpAjaxController.php
 *  *****************************************************************************************************************
 *  @author: Emmanuel Grosdemange <emmanuel.grosdemange57@gmail.com>
 *  @Copyright 2022 Flagstone.
 *  *****************************************************************************************************************
 *  Created: 2022/04/22
 *  ***************************************************************************************************************** */

namespace Flagstone\DatabaseDumpBundle\Controller;

use Flagstone\DatabaseDumpBundle\Entity\DumpResponse;
use Flagstone\DatabaseDumpBundle\Service\DatabaseDump;
use Flagstone\DatabaseDumpBundle\Service\Exceptions\FileSystemExceptions\DirectoryNotCreatedException;
use Flagstone\DatabaseDumpBundle\Service\Exceptions\FileSystemExceptions\FileNotExistsException;
use Flagstone\DatabaseDumpBundle\Service\Exceptions\FileSystemExceptions\InvalidDirectoryParameterException;
use Symfony\Component\DependencyInjection\ParameterBag\ContainerBag;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Exception;

/** *****************************************************************************************************************
 *  Class DatabaseDumpController
 *  -----------------------------------------------------------------------------------------------------------------
 *  Class created to manage ajax call without _locale route at the beginning of the url
 *  @package Flagstone\DatabaseDump\Controller
 *  -----------------------------------------------------------------------------------------------------------------
 *  @Route(
 *      "/dump"
 *  )
 *
 *  ***************************************************************************************************************** */
class DatabaseDumpAjaxController
{
    /** @var ContainerBag  */
    private ContainerBag $params;

    public function __construct(ContainerBag $params)
    {
        $this->params = $params;
    }

    /** *************************************************************************************************************
     *  @param DatabaseDump $dbDump
     *  @return JsonResponse
     *  @throws Exception
     *  -------------------------------------------------------------------------------------------------------------
     *  @Route(
     *      path    = "/dump-make",
     *      name    = "fs_database_dump_make"
     *  )
     *  ************************************************************************************************************* */
    public function makeDump(DatabaseDump $dbDump)
    {
        /** @var DumpResponse $result */
        $result = $dbDump->make();
        return new JsonResponse($result->getArray());
    }

    /** *************************************************************************************************************
     *  @param Request $request
     *  @return JsonResponse
     *  -------------------------------------------------------------------------------------------------------------
     *  @Route(
     *      path    = "/dump-upload",
     *      name    = "fs_database_upload_make"
     *  )
     *  ************************************************************************************************************* */
    public function uploadDump(Request $request)
    {
        /** @var UploadedFile $file */
        $file = $request->files->get('file');
        $directory = $this->params->get('database_dump_dumpDir');

        if (null !== $file) {
            $originalFilename = pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME).'.gz';
        } else {
            return new JsonResponse([
                'status'    => 'false',
                'message'   => 'Unable to move file on the server',
                'error'     => 'The file cannot be uploaded. Verify the PHP post_max_size parameter value (actual : '.ini_get('post_max_size').')'
            ]);
        }

        try {
            $file->move($directory, $originalFilename);
        } catch (FileException $e) {
            return new JsonResponse([
                'status'    => 'false',
                'message'   => 'Unable to move file on the server',
                'error'     => $e->getMessage()
            ]);
        }

        return new JsonResponse(['status' => 'ok']);
    }

    /** *************************************************************************************************************
     *  @param DatabaseDump $dbDump
     *  @param string $dumpFile
     *  @return JsonResponse
     *  -------------------------------------------------------------------------------------------------------------
     *  @Route(
     *      path    = "/dump-restore/{dumpFile}",
     *      name    = "fs_database_dump_restore"
     *  )
     *  ************************************************************************************************************* */
    public function restoreDump(DatabaseDump $dbDump, string $dumpFile)
    {
        $dumpFile = preg_replace('/\~tar\~gz/', '.tar.gz', $dumpFile);
        /** @var DumpResponse $result */
        try {
            $result = $dbDump->restore($dumpFile);
        } catch (DirectoryNotCreatedException|FileNotExistsException|InvalidDirectoryParameterException $e) {
            $exceptionClass = get_class($e);
            if ('DirectoryNotCreatedException' === $exceptionClass) {
                $message = 'Database dump directory not created';
            } elseif ('FileNotExistsException' === $exceptionClass) {
                $message = 'Database dump file not exists';
            } else {
                $message = 'Invalid Database dump directory parameter';
            }

            return new JsonResponse([
                'status'    => 'false',
                'message'   => $message,
                'error'     => $e->getMessage()
            ]);
        } catch (Exception $e) {
            return new JsonResponse([
                'status'    => 'false',
                'message'   => 'Unknown error',
                'error'     => sprintf('File: %s<br />Line: %d<br />Message: %s', $e->getFile(), $e->getLine(), $e->getMessage())
            ]);
        }

        return new JsonResponse($result->getArray());
    }
}
