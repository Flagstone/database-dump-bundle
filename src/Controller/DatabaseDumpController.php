<?php
/** *****************************************************************************************************************
 *  DatabaseDumpController.php
 *  *****************************************************************************************************************
 *  @author: Emmanuel Grosdemange <emmanuel.grosdemange57@gmail.com>
 *  @Copyright 2022 Flagstone.
 *  *****************************************************************************************************************
 *  Created: 2022/04/22
 *  ***************************************************************************************************************** */

namespace Flagstone\DatabaseDumpBundle\Controller;

use Flagstone\DatabaseDumpBundle\Service\DatabaseDump;
use Psr\Container\ContainerInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Twig\Environment;

/** *****************************************************************************************************************
 *  Class DatabaseDumpController
 *  @package Flagstone\DatabaseDump\Controller
 *  -----------------------------------------------------------------------------------------------------------------
 *  @Route(
 *      "/dump"
 *  )
 *
 *  ***************************************************************************************************************** */
class DatabaseDumpController
{
    private ContainerInterface $container;
    private Environment $twig;

    public function __construct(ContainerInterface $container, Environment $twig)
    {
        $this->container = $container;
        $this->twig = $twig;
    }

    /** *************************************************************************************************************
     * @param DatabaseDump $dbDump
     * @return Response
     *  -------------------------------------------------------------------------------------------------------------
     * @throws \Twig\Error\LoaderError
     * @throws \Twig\Error\RuntimeError
     * @throws \Twig\Error\SyntaxError
     * @Route(
     *      path    = "/dump-list",
     *      name    = "fs_database_dump_list"
     *  )
     *  ************************************************************************************************************
     */
    public function dumpList(DatabaseDump $dbDump)
    {
        return $this->render(
            '@FlagstoneDatabaseDump/list.html.twig',
            [
                'dumps'     => $dbDump->getDumps()
            ]
        );
    }

    /** *************************************************************************************************************
     * @param DatabaseDump $dbDump
     * @param string $dump
     * @return BinaryFileResponse
     *  -------------------------------------------------------------------------------------------------------------
     * @Route(
     *      path    = "/dump-download/{dump<dump_\d{14}_\d{14}~tar~gz>}",
     *      name    = "fs_database_dump_download"
     *  )
     *  ************************************************************************************************************  */
    public function dumpDownload(DatabaseDump $dbDump, string $dump): BinaryFileResponse
    {
        $dump = preg_replace('/\~tar\~gz/', '.tar.gz', $dump);
        $fullPath = $dbDump->getDumpDir().$dump;

        $response = new BinaryFileResponse($fullPath);
        $response->setContentDisposition(
            ResponseHeaderBag::DISPOSITION_ATTACHMENT,
            $dump
        );
        return $response;
    }

    /** *************************************************************************************************************
     *  @param DatabaseDump $dbDump
     *  @param string $dump
     *  @return RedirectResponse
     *  -------------------------------------------------------------------------------------------------------------
     *  @Route(
     *      path    = "/dump-remove/{dump<dump_\d{14}_\d{14}~tar~gz>}",
     *      name    = "fs_database_dump_remove"
     *  )
     *  ************************************************************************************************************* */
    public function dumpRemove(DatabaseDump $dbDump, string $dump): RedirectResponse
    {
        $dump = preg_replace('/\~tar\~gz/', '.tar.gz', $dump);
        $fullPath = $dbDump->getDumpDir().$dump;

        if (file_exists($fullPath)) {
            unlink($fullPath);

            return $this->redirect(
                $this->generateUrl('fs_database_dump_list')
            );
        }

        throw new NotFoundHttpException('This dump doesn\'t exists.');
    }

    protected function generateUrl(string $route, array $parameters = [], int $referenceType = UrlGeneratorInterface::ABSOLUTE_PATH): string
    {
        return $this->container->get('router')->generate($route, $parameters, $referenceType);
    }

    /**
     * @throws \Twig\Error\SyntaxError
     * @throws \Twig\Error\RuntimeError
     * @throws \Twig\Error\LoaderError
     */
    protected function renderView(string $view, array $parameters = []): string
    {
        foreach ($parameters as $k => $v) {
            if ($v instanceof FormInterface) {
                $parameters[$k] = $v->createView();
            }
        }

        return $this->twig->render($view, $parameters);
    }

    /**
     * @throws \Twig\Error\SyntaxError
     * @throws \Twig\Error\RuntimeError
     * @throws \Twig\Error\LoaderError
     */
    protected function render(string $view, array $parameters = [], Response $response = null): Response
    {
        $content = $this->renderView($view, $parameters);

        if (null === $response) {
            $response = new Response();
        }

        if (200 === $response->getStatusCode()) {
            foreach ($parameters as $v) {
                if ($v instanceof FormInterface && $v->isSubmitted() && !$v->isValid()) {
                    $response->setStatusCode(422);
                    break;
                }
            }
        }

        $response->setContent($content);

        return $response;
    }

    /**
     * Returns a RedirectResponse to the given URL.
     */
    protected function redirect(string $url, int $status = 302): RedirectResponse
    {
        return new RedirectResponse($url, $status);
    }
}