<?php
/** *****************************************************************************************************************
 *  FlagstoneDatabaseDumpBundle.php
 *  *****************************************************************************************************************
 *  @copyright 2022 Flagstone
 *  @author Emmanuel Grosdemange <emmanuel.grosdemange57@gmail.com>
 *  *****************************************************************************************************************
 *  Created: 2022/04/22
 *  ***************************************************************************************************************** */

namespace Flagstone\DatabaseDumpBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

/** *****************************************************************************************************************
 *  Class FlagstoneDatabaseDumpBundle
 *  -----------------------------------------------------------------------------------------------------------------
 *  This bundle allows you to perform backups and restores of an application's database, as well as documents
 *  downloaded by users, whose links are stored in the database.
 *  -----------------------------------------------------------------------------------------------------------------
 *  @package Flagstone\DatabaseDumpBundle
 *  ***************************************************************************************************************** */
class FlagstoneDatabaseDumpBundle extends Bundle
{
    const DOMAIN_TRANS = 'DatabaseDump';

    public static function getContext()
    {
        return 'flagstone-database-dump';
    }
}