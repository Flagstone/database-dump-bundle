let Encore = require('@symfony/webpack-encore')

Encore
  .setOutputPath('src/Resources/public/build')
  .setPublicPath('/build')

  .addEntry('fs_database_dump_js', './assets/js/dump_database.js')
  .addStyleEntry('fs_database_dump_css', './assets/scss/fs_database_dump.scss')

  .disableSingleRuntimeChunk()
  .cleanupOutputBeforeBuild()
  .enableBuildNotifications()
  .enableSourceMaps(!Encore.isProduction())
  .enableVersioning(Encore.isProduction())

  .enableSassLoader()
  .enablePostCssLoader((options) => {
    options.config = {
      path: 'src/Resources/config/postcss.config.js'
    }
  })


module.exports = Encore.getWebpackConfig()
